var WebSocketClient = require('websocket').client;
var stdin = process.stdin;
var client = new WebSocketClient();

stdin.setEncoding( 'utf8' );

client.on('connectFailed', function(error) {
    console.log(error.toString());
});

client.on('connect', function(connection) {
    console.log('Conectado!');
    stdin.on( 'data', function( key ){
	    connection.sendUTF(key);
	});
    connection.on('error', function(error) {
        console.log(error.toString());
    });
    connection.on('close', function() {
        console.log('Conexão caiu');
    });
    connection.on('message', function(message) {
        console.log('dado do servidor: '+message.utf8Data);
    });
});
client.connect('ws://localhost:1337');