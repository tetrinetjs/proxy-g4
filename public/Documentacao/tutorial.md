# Introdução
Essa documentação pretende mostrar o funcionamento básico de nosso servidor proxy, afim de auxiliar a implementação de um client js.

## Conectando a um servidor TetriNET através do proxy

Para se conectar a um servidor TetriNET você deve usar WebSockets, o servidor não devera responder requisições em *raw socket*.
Por padrão a porta do servidor é 1337. Exemplo de código em JavaScript para estabelecer a conexão:

socket = new WebSocket("ws://127.0.0.1:1337");

O suporte a WebSockets já é amplamente implementado nos navegadores atuais, e deverá funcionar em sem problemas nas versões 10 ou superiores
do Internet Explorer, ou versões superior a 4 do Firefox.
Após estabelecer a conexão com servidor proxy devemos enviar um comando para se conectar ao servidor TetriNET, passando o ip e a porta
como parâmetros.

socket.send("connect 127.0.0.1 31457");

Após isso estamos conectados ao servidor TetriNET através do proxy. Após estabelecer a conexão é importante enviar a string de
autenticação do TetriNET para que a conexão seja mantida, e também deveremos criar um método de callback para ser chamado ao receber
mensagens do servidor:

socket.onmessage = function(event) {
  //trata a mensagem aqui
};

Pronto!Com isso já temos o básico para a criação de um client conectado a um proxy usando WebSockets.