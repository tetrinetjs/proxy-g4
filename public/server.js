var WebSocketServer = require('websocket').server;
var http = require('http');
var net = require('net');

var server = http.createServer(function(request, response) {
  
});

server.listen(1337, function() { });

wsServer = new WebSocketServer({
  httpServer: server
});
var connections = [];
var tetrinetSrvs = [];
wsServer.on('request', function(request) { 
  connections[request.key] = request.accept(null, request.origin);
  /*conecta no servidor tetrinet */
  tetrinetSrvs[request.key] = new net.Socket();
  

  tetrinetSrvs[request.key].on('data', function(data) {
	//entrada de dados do tetrinet
	console.log('Recebido do tetrinet: ' + data);
	connections[request.key].sendUTF(data);
  });

  tetrinetSrvs[request.key].on('close', function() {
	console.log('Conexão com servidor tetrinet perdida');
	// evento da conexão fechou console.log('');
  });
  
  tetrinetSrvs[request.key].on('error', function(ex) {
	console.log('Conexão caiu inesperadamente');
	connections[request.key].sendUTF('Conexão com servidor tetrinet perdida.');
	connections[request.key].close();
	delete connections[request.key];
	delete tetrinetSrvs[request.key];
  });
	
  connections[request.key].on('message', function(message) {
     console.log('Recebido do cliente: '+message.utf8Data);	
	 cmd = message.utf8Data.trim();
	 if (cmd.split(' ')[0] == 'connect'){
		 var ipTetrinet = cmd.split(' ')[1];
		 var porta = cmd.split(' ')[2];
		 tetrinetSrvs[request.key].connect(porta, ipTetrinet, function() {
				console.log('Conectado no servidor tetrinet');
		 });
		 return;
	 }
	 try{
	     tetrinetSrvs[request.key].write(message.utf8Data);
	 }
	 catch(e){
		 connections[request.key].sendUTF(e);
	 }
  });

  connections[request.key].on('close', function(connection) {
    delete connections[request.key];
	delete tetrinetSrvs[request.key];
  });
});